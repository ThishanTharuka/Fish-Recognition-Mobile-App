class FishSpecies {
  final String scientificName;
  final String commonName;
  final String imagePath;

  FishSpecies({
    required this.scientificName,
    required this.commonName,
    required this.imagePath,
  });
}
